// Ionic Starter App

angular.module('starter', ['ionic','starter.controllers','angularMoment'])
.run(function($ionicPlatform, amMoment, $ionicPopup) {
	$ionicPlatform.ready(function() {
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		if (window.StatusBar) {
			StatusBar.styleDefault();
		}

	});
	amMoment.changeLocale('pt-br');
})

.constant('angularMomentConfig', {
    timezone: 'America/Sao_Paulo' // Optional
})

.config(function($stateProvider, $urlRouterProvider) {

    // $httpProvider on function()
    // $httpProvider.defaults.useXDomain = true;

	$stateProvider

	.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/menu.html',
		controller: 'MenuController'
	})

	.state('app.main', {
		url: '/main',
		views: {
			'content': {
				templateUrl: 'templates/main.html',
				controller: 'MainController'
			}
		}
	})

	.state('app.result', {
		url: '/result/:hash',
		params: { hash: 123456 },
		views: {
			'content': {
				templateUrl: 'templates/result.html',
				controller: 'ResultController'
			}
		}
	})

    .state('app.shares', {
        url: '/shares',
        views: {
            'content': {
                templateUrl: 'templates/shares.html',
                controller: 'SharesController'
            }
        }
    })

    .state('app.about', {
        url: '/about',
        views: {
            'content': {
                templateUrl: 'templates/about.html',
                controller: 'AboutController'
            }
        }
    })

	$urlRouterProvider.otherwise('/app/main');

})