angular.module('starter.controllers', [])

.controller('MenuController', function($scope) {



})

.controller('MainController', function($scope, $state) {

	/**
	 *
	 * Algorythm
	 *
	 * Urgência: u
	 * Complexidade: c
	 * Empolgação: e
	 * Habilidade: h
	 * Vontade: v
	 *
	 * iVDM = ((u*c)/(e+h+v))*10
	 * If iVDM < 0, iVDM * 10
	 * If iVDM > 100, iVDM = 100
	 *
	 */

	$scope.data = {
		range1: 1,
		range2: 1,
		range3: 1,
		range4: 1,
		range5: 1,
	};

	$scope.doCalc = function() {

		var u = parseInt( $scope.data.range1 ),
			c = parseInt( $scope.data.range2 ),
			e = parseInt( $scope.data.range3 ),
			h = parseInt( $scope.data.range4 ),
			v = parseInt( $scope.data.range5 );

		function calc() {

			var i = Math.ceil( (( u * c ) / ( e + h + v )) * 10 );

			if ( i < 0 )         i * 10;
			else if ( i > 100 )  i = 100;

			// Save iVDM and numbers on DB via Ajax JSONP

			return i;
		}

		var result = calc();
		$state.go('app.result', { hash: result });

	}

    $scope.setRangeToOne = function(rangeID) {
        $scope.data[rangeID] = 1;
    }

})

.controller('ResultController', function($scope, $state, $stateParams, $ionicPopup, $http, $ionicLoading) {

    var numMin = 0,
        numMax = 3,
        num = Math.floor(Math.random() * numMax) + numMin;

    // 90% de chances VDM
    var valuation_90 = [
        "Na boa, mexe com isso não.",
        "Hum... melhor deixar esta atividade para outro dia!",
        "Tem certeza que vai fazer isso?",
        "Que tal pedir ajuda aos universitários?"
    ];
    // 75% de chances VDM
    var valuation_75 = [
        "Espero que você goste de um belo\nElefante Branco enfeitando sua sala...",
        "Hum... talvez na carpintaria você se dê bem!\nAqui parece que não.",
        "Fuja para as montanhas enquanto ainda há tempo!!",
        "Considere pedir ajuda a alguém com experiência."
    ];
    // 50% de chances VDM
    var valuation_50 = [
        "Hum...\nPode até ser que dê certo.",
        "'Isso vai dar certo  ou não me chamo João!'\nPrazer, meu nome é Paulo.",
        "Se existe a possibilidade de desistir,\ndesista agora!",
        "Cara..\nTô sentindo um cheirinho estranho de m*..."
    ];
    // 30% de chances VDM
    var valuation_30 = [
        "Vai que dá!\nOu não.",
        "Uau!\nAs chances estão boas, heim!?",
        "Você tem boas chances de dar certo!\nDe dar errado também.",
        "Já ouviu falar na Lei de Murphy?"
    ];
    // 15% de chances VDM
    var valuation_15 = [
        "Como diria Dominguinhos:\n'Isso aqui tá muito bom, isso aqui tá bom demais!'",
        "Levante as velas e vá para o mar, marujo!\nOs ventos estão à favor.",
        "Pode ir que dá!",
        "'Espere pelo melhor ma\nesteja preparado para o pior.'",
    ];

    var ivdm = $stateParams.hash,
        valuation = '';

    if ( ivdm >= 90 )         valuation = valuation_90[num];
    else if ( ivdm >= 75 )    valuation = valuation_75[num];
    else if ( ivdm >= 50 )    valuation = valuation_50[num];
    else if ( ivdm >= 30 )    valuation = valuation_30[num];
    else if ( ivdm >= 15 )    valuation = valuation_15[num];
    else                      valuation = valuation_15[num];

    $scope.results = {
        hash: ivdm,
        valuation: valuation
    };

    // Used on SharesController
    // publicResults = $scope.results;

    $scope.doShare = function(ivdm) {

        $scope.data = {
            charsLeft: 65
        };

        var shareTitlePopup = $ionicPopup.show({
            title: 'Escreva um título para o compartilhamento',
            // subTitle: 'O título será exibido junto com o iVDM',
            template: '<textarea cols="" rows="2" ng-change="limitShareTitle()" class="padding" style="resize: none;" placeholder="O título será exibido junto com o iVDM" ng-model="data.title" max-length="65"></textarea> <span class="titleCharsLeft">{{ data.charsLeft }} caracteres</span>',
            scope: $scope,
            buttons: [
                {
                    text: 'Cancelar',
                    type: 'button-light button-small'
                },
                {
                    text: 'Compartilhar',
                    type: 'button-balanced',
                    onTap: function(e) {
                        if ( !$scope.data.title ) {
                            // Don't allow the user to send without write a title for sharing
                            e.preventDefault();
                        } else {

                            $ionicLoading.show();

                            var the_url = window.location.origin;

                            if ( the_url == 'http://192.56.1.30' )
                                var api_consumer_url = the_url + '/ivdm.lucasmoreira.com.br-site/public/api/newShare/' + ivdm + '/' + encodeURI($scope.data.title);
                            else
                                var api_consumer_url = 'https://ivdm.lucasmoreira.com.br/api/newShare/' + ivdm + '/' + encodeURI($scope.data.title);

                            $http.get( api_consumer_url ).then(function(response) {
                                if ( response.status == 200)
                                    $state.go('app.shares', { publicResults: $scope.results });
                            });

                        }
                    }
                }
            ]
        });

        // shareTitlePopup.then(function(response) {
            // console.log('Tapped!' + $scope.data.title);
        // });

    }

    $scope.limitShareTitle = function() {
        var max_limit = 65,
            title = $scope.data.title,
            cur_length = ( max_limit - title.length );

        if ( cur_length > 0 )
            $scope.data.charsLeft = cur_length;
        else {
            $scope.data = {
                title: title.substring(0,65),
                charsLeft: 0
            }
        }

    }

})

.controller('SharesController', function($scope, $http, $ionicPopup, $ionicLoading) {

    /**
     * To enable CORS (Cross-Origin Resource Sharing):
     * On API server (API provider):
     *
     * # cd /etc/apache2/mods-available
     * # a2enmod headers
     * # /etc/init.d/apache2 restart
     *
     * <IfModule mod_headers.c>
     *    Header add Access-Control-Allow-Origin "*"
     *    Header add Access-Control-Allow-Headers "origin, x-requested-with, content-type"
     *    Header add Access-Control-Allow-Methods "PUT, GET, POST, DELETE, OPTIONS"
     * </IfModule>
     *
     */

    $ionicLoading.show();

    var the_url = window.location.origin,
        publicResults = '';

    if ( the_url != 'http://192.56.1.30' ) {

        // navigator.connection.type works only on device
        // It could returns:
        // none, wifi and cellular (for 3G connection)

        // Chrome provide window.navigator.onLine function to check internet connection
        // It returns true or false

        // Other good-to-know functions:
        // window.navigator.appCodeName returns Mozilla
        // window.navigator.appName returns Netscape
        // This two functions above returns same result on browser (development) and on device (tested on iPhone 4S)

        // window.connection will return "undefined" in browser and device

        if ( window.Connection ) {

            if ( navigator.connection.type == Connection.NONE ) {
                $ionicPopup.alert({
                    title: 'Whoops',
                    content: 'Você precisa de uma conexão com a internet.'
                })
                // .then(function(result) {
                    // if ( !result ) {
                        // ionic.Platform.exitApp();
                    // }
                // })
            }
        }

    }

    // max 65 chars on title

    // console.log( 'Results: ' + publicResults );

    if ( publicResults != '' )
        $scope.shared = publicResults;

    if ( the_url == 'http://192.56.1.30' )
        var api_consumer_url = the_url + '/ivdm.lucasmoreira.com.br-site/public/api/getShared';
    else
        var api_consumer_url = 'https://ivdm.lucasmoreira.com.br/api/getShared';

    $http.get( api_consumer_url ).then(function(response) {
        // console.log(response);
        $scope.shared = response.data;

        $ionicLoading.hide();
    });

    $scope.doRefresh = function() {
        $ionicLoading.show();

        $http
        .get( api_consumer_url )
        .success(function(newItems) {
            $scope.shared = newItems;

            $ionicLoading.hide();

        })
        .finally(function() {
            // Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

})

.controller('AboutController', function() {

})
